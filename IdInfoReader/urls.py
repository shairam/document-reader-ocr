from django.urls import path
from . import views

urlpatterns = [
    path('', views.upload, name='upload'),
    path('details', views.details, name='details'),
    path('submission', views.submission, name='submission')
]