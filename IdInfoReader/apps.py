from django.apps import AppConfig


class IdinforeaderConfig(AppConfig):
    name = 'IdInfoReader'
