from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from .src.aadhar import Aadhar
from .src.pan_card import PanCard
from .src.driving_license import DrivingLicense

import os

_info = {}

def upload(request):
    
    return render(request, 'upload.html', {'color' : '#eeeeee'})

def details(request):

    ui = {}
    if request.method == 'POST':
        context = {}
        info = {}
        uploaded_files = request.FILES.getlist('document')
        context['names'] = []
        context['urls'] = []
        context['selection'] = request.POST.get('selection')            

        fs = FileSystemStorage()
        names = []
        for f in uploaded_files:
            print(fs.listdir(settings.MEDIA_ROOT))
            if fs.exists(f.name):
                os.remove(os.path.join(settings.MEDIA_ROOT, f.name))
            names.append(fs.save(f.name, f))

        print(names)
        if context['selection'] == "Aadhar Card":
            aadhar = Aadhar()
            data = aadhar.get_text(names)
        elif context['selection'] == 'Pan Card':
            pancard = PanCard()
            data = pancard.get_text(names)
        elif context['selection'] == 'Driving License':
            dLicense = DrivingLicense()
            data = dLicense.get_text(names)

        info = data[0]
        ui = data[1]

    ui['color'] = '#fde9ce'

    return render(request, 'details.html', {'info':info, 'ui' : ui, 'idType' : context['selection']})

def submission(request):
    return render(request, 'submission.html')