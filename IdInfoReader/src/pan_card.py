import cv2
import pytesseract
import re

import string

import pandas as pd
import numpy as np

from .del_ch import Del
from .pdf_to_jpg import PdfToJpg

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


DD = Del()

class PanCard:

    def __init__(self):
        self.image_name = []
        self.n = pd.read_csv('db\\Indian_names_db.csv')
        self.names = np.concatenate(self.n.values.tolist())
        self.regNames = []
        self.regFatherNames = []

    def get_text_from_img(self, image_name):

        img = cv2.imread('media\\' + image_name)

        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        gray, img_bin = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        gray = cv2.bitwise_not(img_bin)

        kernel = np.ones((2, 1), np.uint8)
        img = cv2.erode(gray, kernel, iterations=1)
        img = cv2.dilate(img, kernel, iterations=1)
        out_below = pytesseract.image_to_string(img)
        return out_below

    def get_text(self, image_name):
        if image_name[0][-3:] == "pdf":
            pdf_jpg = PdfToJpg()
            self.image_name = pdf_jpg.convert_pdf_jpg(image_name[0])
        else:
            self.image_name = image_name


        text = []
        for image in self.image_name:
            text.append(self.get_text_from_img(image))

        info = {'Name' : '', 'Father Name' : '', 'PAN no' : '', 'DOB' : ''}

        for txt in text:
            print(txt)
            splitted = txt.split('\n\n')
            
            for i in range(len(splitted)):

                res = re.search(r"^[a-zA-Z0-9_]*$", splitted[i])
                if res != None:
                    info['PAN no'] = res.group()
                    
                res = re.search(r'(\d+/\d+/\d+)', splitted[i])
                if res != None:
                    info['DOB'] = res.group(1)

            nameInd = 0
            fatherInd = -1
            dateInd = -1
            
            if "father" in txt.lower():
                fatherInd = txt.lower().index('father')
            if "date" in txt.lower():
                dateInd = txt.lower().index("date")

            if nameInd != -1 and fatherInd != -1:
                for i in txt[nameInd:fatherInd].split():
                    word = i.lower().strip()
                    if word in self.names:
                        self.regNames.append(word.capitalize())
            if fatherInd != -1 and dateInd != -1:
                for i in txt[fatherInd:dateInd].split():
                    word = i.lower().strip()
                    if word in self.names:
                        self.regFatherNames.append(word.capitalize())
            if dateInd != -1:
                res = re.search(r'(\d+/\d+/\d+)', txt[dateInd:])
                if res != None:
                    info['DOB'] = res.group(1)

        if len(self.regNames) != 0:
            info['Name'] = self.regNames[0]
        if len(self.regFatherNames) != 0:
            info['Father Name'] = self.regFatherNames[0]


        return (info, {'images' : self.image_name})
