from pdf2image import convert_from_path
import os
from django.conf import settings

class PdfToJpg:
    def __init__(self):
        self.poppler_path = r'D:\downloads\packages\poppler-21.03.0\Library\bin'

    def convert_pdf_jpg(self, path):
        path2 = os.path.join(settings.MEDIA_ROOT, path)
        print(path2)
        pages = convert_from_path(pdf_path=path2, poppler_path=self.poppler_path)

        image_list = []
        i = 1
        for page in pages:
            name = path[:-4] + "_" + str(i) + ".jpg"
            page.save('media\\' + name, "JPEG")
            image_list.append(name)
            i += 1
        return image_list