import cv2
import pytesseract
import re

import string
import pandas as pd
import numpy as np

from .del_ch import Del
from .pdf_to_jpg import PdfToJpg

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

DD = Del()

regNames = []

class Aadhar:

    def __init__(self):
        self.image_name = []
        self.n = pd.read_csv('db\\Indian_names_db.csv')
        self.names = np.concatenate(self.n.values.tolist())
        self.regNames = []

    def get_text_from_img(self, image_name):

        img = cv2.imread('media\\' + image_name)

        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        gray, img_bin = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        gray = cv2.bitwise_not(img_bin)

        kernel = np.ones((2, 1), np.uint8)
        img = cv2.erode(gray, kernel, iterations=1)
        img = cv2.dilate(img, kernel, iterations=1)
        out_below = pytesseract.image_to_string(img)
        return out_below

    def get_text(self, image_name):
        if image_name[0][-3:] == "pdf":
            pdf_jpg = PdfToJpg()
            self.image_name = pdf_jpg.convert_pdf_jpg(image_name[0])
        else:
            self.image_name = image_name

        text = []
        for image in self.image_name:
            text.append(self.get_text_from_img(image))

        info = {'Name' : '','Gender' : '', 'Aadhar no' : '', 'DOB' : '', 'Address' : ''}

        for txt in text:
            splitted = txt.split('\n\n')
            
            for i in range(len(splitted)):

                print(i, ":", splitted[i], ":")
                if "DOB" in splitted[i].upper():
                    dobInd = splitted[i].index("DOB")
                    info['DOB'] = splitted[i][dobInd+4:dobInd+16]
                    info['DOB'] = info['DOB'].replace(':', '')
                    info['DOB'] = info['DOB'].strip()
                if "father" in splitted[i].lower():
                    info['Father Name'] = splitted[i][9:]
                if "male" in splitted[i].lower() or "female" in splitted[i].lower():
                    if '/' in splitted[i][-10:]:
                        ind = splitted[i][-10:].index('/')
                    elif '|' in splitted[i][-10:]:
                        ind = splitted[i][-10:].index('|')
                    else:
                        ind = 0
                    info['Gender'] = splitted[i][-10:][ind+2:]
                if "address" in splitted[i].lower():
                    ind = splitted[i].lower().index('address')
                    info['Address'] = splitted[i][ind+8:].strip().replace('\n', ' ')
                num = splitted[i].translate(DD)
                if(len(num) >= 12):
                    info['Aadhar no'] = num[:12]
                    info['Aadhar no'] = num[:4] + " " + num[4:8] + " " + num[8:]
                # print(str(i) + " : " + num)
            if "DOB" in txt.upper():
                for i in txt[:txt.upper().index('DOB')].split():
                    word = i.lower().strip()
                    if word in self.names:
                        self.regNames.append(word.capitalize())
        if len(self.regNames) != 0:
            info['Name'] = self.regNames[0]

        return (info, {'images' : self.image_name})
