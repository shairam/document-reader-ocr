import cv2
import pytesseract
from PIL import Image

import re
import string

import pandas as pd
import numpy as np

from .del_ch import Del
from .pdf_to_jpg import PdfToJpg

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


DD = Del()

class DrivingLicense:

    def __init__(self):
        self.image_name = []
        self.n = pd.read_csv('db\\Indian_names_db.csv')
        self.names = np.concatenate(self.n.values.tolist())
        self.regNames = []
        self.info = {'Name' : '', 'Father Name' : '', 'DL no' : '', 'DOB' : '', 'Date of issue' : '', 'Valid till' : ''}

    def binarize(self, image_to_transform, threshold):
        output_image = image_to_transform.convert("L")
        for x in range(output_image.width):
            for y in range(output_image.height):
                if output_image.getpixel((x,y)) < threshold:
                    output_image.putpixel((x,y), 0)
                else:
                    output_image.putpixel((x,y), 255)
        return output_image

    def get_text_from_img(self, image_name):
        binImage = self.binarize(Image.open('media\\' + image_name), 64)
        text = pytesseract.image_to_string(binImage)
        return text


    def get_text(self, image_name):
        if image_name[0][-3:] == "pdf":
            pdf_jpg = PdfToJpg()
            self.image_name = pdf_jpg.convert_pdf_jpg(image_name[0])
        else:
            self.image_name = image_name


        text = []
        for image in self.image_name:
            text.append(self.get_text_from_img(image))

        for txt in text:
            
            tnInd = 0
            if 'TN' in txt:
                tnInd = txt.index('TN')
            
            self.info['DL no'] = txt[tnInd:tnInd+16]
            res = re.findall(r"[\d]{1,2}-[\d]{1,2}-[\d]{2,4}", txt)
            if len(res) >= 3:
                self.info['Date of issue'] = res[0]
                self.info['Valid till'] = res[1]
                self.info['DOB'] = res[2]

                lastDateInd = txt.index(res[-1])
                self.regNames = txt[lastDateInd+len(res[-1]):].split()

            temp = []
            for name in self.regNames:
                if name.lower() in self.names:
                    temp.append(name)

            if(len(temp) >= 2):
                self.info['Name'] = temp[0]
                self.info['Father Name'] = temp[1]

        return (self.info, {'images' : self.image_name})
